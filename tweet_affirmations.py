import tweepy

from credentials import *

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)

#setting up API object
api = tweepy.API(auth)

# open() function opens a file and returns it as a file object
# 'r' - opens a file for reading
my_file = open('affirmations.txt', 'r')

# readlines() returns all lines in the file as a list, where each line is an itme in the list object
file_lines = my_file.readlines()

# closes the opened file
print(file_lines[3])

# Updates the authenticating user’s current status, also known as Tweeting.
api.update_status(file_lines[3])