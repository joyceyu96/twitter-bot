import tweepy
import time 
from credentials import *

print("this is my twitter bot")

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)

#setting up API object
api = tweepy.API(auth)

# accessing all the tweets that mention my account


FILE_NAME = 'last_seen_id.txt'

def retrieve_last_seen_id(file_name):
    f_read = open(file_name, 'r')
    last_seen_id = int(f_read.read().strip())
    f_read.close()
    return last_seen_id

def store_last_seen_id(last_seen_id, file_name):
    f_write = open(file_name, 'w')
    f_write.write(str(last_seen_id))
    f_write.close()
    return

def reply_to_tweets():
    print("Retrieving and replying to tweets")
    
    #retrieving last seen ID from file
    last_seen_id = retrieve_last_seen_id(FILE_NAME)


    mentions = api.mentions_timeline(since_id=last_seen_id)

    for mention in reversed(mentions):
        print(str(mention.id) + ' - ' + mention.text)

        #setting last seen id to the current mention id
        last_seen_id = mention.id
        store_last_seen_id(last_seen_id, FILE_NAME)

        if '#helloworld' in mention.text.lower():
            print("found hello world, responding back")
            api.update_status(status=('@' + mention.user.screen_name + ' hello back to you!'), in_reply_to_status_id=mention.id)
            
reply_to_tweets()